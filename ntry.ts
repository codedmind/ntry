import { ensureFileSync, ensureDirSync, existsSync } from "https://deno.land/std@0.108.0/fs/mod.ts";
//mport { datetime } from "https://deno.land/x/ptera@v1.0.0-beta/mod.ts";
import { parse, format, difference } from "https://deno.land/std@0.108.0/datetime/mod.ts";

class NTRY {
    forms:any[] = [];
    values:any = {};
    path:string = "";

    exp_char(){
        return this.values.exp_char;
    }

    tags_char(){
        return this.values.tags_char;
    }

    state_char(){
        return this.values.state_char;
    }

    constructor(path:string){
        this.values.tags_char = "@";
        this.values.state_char = "#";
        this.values.xp_char = "XP:";
        this.path = path;
        this.load();
    }

    getDiff(nthen: any, nnow: any){
        let diff = difference(nthen, nnow);
        if(diff.days == 0) return "new";
        else if(diff.days == 1) return "added yesterday";
        else return `added ${diff.days} days ago`

    }

    printForm(form:any){
        let index = this.forms.indexOf(form);
        let nthen = form.dt
        let nnow = new Date();
        let ts = this.getDiff(nthen, nnow);
        console.log("| #"+index+" "+form.header+" ["+form.state+"] ("+ts+")");
        if(form.text.includes("\n")){
            for(const line of form.text.split("\n")){
                console.log("| "+line); 
            }
        } else console.log("| "+form.text);

        if(form.checklist.length > 0){
            for(const check of form.checklist){
                console.log("| - ["+check.state+"] "+check.text)
            }
        }
    }

    add_form(text:string, index = -1){
        let lines = text.split("\n");
        let header = lines.shift();
        if(header != undefined && header != ""){
            if(header == "[[META]]"){
                for(const ln of lines){
                    if(ln.trim() != "" && ln.includes("=")){
                        let key:string = ln.split("=")[0].trim();
                        let val:string = ln.split("=")[1].trim();
                        this.values[key] = val;
                    }
                }
            }else{
                let tags:string[] = [];
                let dt = undefined;
                let state:string = "active";
                let mbody = lines.join("\n");
                let checklist = [];

                for(const word of text.split(" ")){
                    if(word.startsWith(this.tags_char())) tags.push(word);

                    if(word.startsWith(this.state_char())) {
                        state = word.slice(this.state_char().length);
                        mbody = mbody.replace(word, "");
                    }

                    if(word.startsWith(this.exp_char())) {
                        state = word.slice(this.exp_char().length);
                        mbody = mbody.replace(word, "");
                    }
                }

                for(const line of text.split("\n")){
                    if(line.startsWith("- [")){
                        let state = line.split("[")[1].split("]")[0];
                        let text = line.split("]")[1];
                        checklist.push({state: state.trim(), text: text.trim()});
                        mbody = mbody.replace(line, "");
                    }
                }

                if(header.includes("@")){
                    dt = parse(header.split("@")[1].trim(), "HH:mm dd/MM/yyyy")
                    header = header.split("@")[0].trim()
                } else {
                    dt = new Date();
                }

                let newForm = {
                    header: header, 
                    text: mbody.trim(), 
                    raw_text: lines.join("\n"), 
                    tags: tags, 
                    dt: dt, 
                    state: state,
                    checklist: checklist
                }

                if(index < 0) this.forms.push(newForm);
                else this.forms[index] = newForm;
            }

        }
    }

    load(){
        const text = Deno.readTextFileSync(this.path);
        for(const bodies of text.split("---")){ this.add_form(bodies.trim()); }
    }

    saveFriendly(form:any){
        let out:string;
        let ts = format(form.dt, "HH:mm dd/MM/yyyy")
        out = form.header + " @ "+ts+"\n" + form.raw_text
        return out;
    }
    save(){
        let newBody = "";
        if(Object.entries(this.values).length > 0){
            newBody = "[[META]]\n";
            for(let [key, val] of Object.entries(this.values)){
                newBody = newBody + "\n" + key +" = "+val;
            }
        }

        for(let form of this.forms){
            let ts = format(form.dt, "HH:mm dd/MM/yyyy")
            newBody = newBody + "\n---\n" + this.saveFriendly(form)//form.header + " @ "+ts+"\n" + form.raw_text
        }
        Deno.writeTextFileSync(this.path, newBody);
    }

    filtered_forms(opt:any){
        let out:any[] = [];
        for (let form of this.forms){
            let addit = false;
            if(opt.contains != undefined && (form.header.includes(opt.contains))) addit = true;
            if(addit) out.push(form);
        }
        return out
    }
}

class Opt {
    commands:string[] = [];
    flags:any = {};

    constructor(){
        for(let opt of Deno.args){
            if(opt.startsWith("-") && (opt.includes("="))){
                this.flags[opt.split("=")[0].replaceAll("-", "")] = opt.split(":")[1]
            }else if(opt.startsWith("-") && (opt.includes(":"))){
                this.flags[opt.split(":")[0].replaceAll("-", "")] = opt.split(":")[1]  
            } else {
                this.commands.push(opt);
            }
        }
    }

    flag(name:string, def?:string){
        if(this.flags[name] != undefined) return this.flags[name];
        return def;
    }
}
function detectFirstRun(path:string){
    return existsSync(path);
}

async function main(){
    var path: string | undefined;
    var home = Deno.env.get("HOME");
    if (home == undefined) home = "";
    home = home+"/";
    if(Deno.env.get("NTRY_PATH") != undefined) path = Deno.env.get("NTRY_PATH");
    if(path == undefined) path = ".ntry.d/";
    if(!detectFirstRun(home+path)){
        console.log("First run!");
        ensureDirSync(home+path);
        ensureFileSync(home+path+"main.ntry");
    }

    const opt = new Opt();
    const ntry = new NTRY(home+path+"main.ntry");
    let editor = "nano";
    if(ntry.values.editor != undefined) editor = ntry.values.editor;
    let cmd = opt.commands.shift();
    let args = opt.commands;
    switch(cmd){
        case undefined:
        case "write": {
            if(args.length == 0){
                ensureFileSync(home+path+"editing.txt");   

                const p = Deno.run({
                    cmd: [editor, home+path+"editing.txt"],
                });

                const { code } = await p.status(); 
                p.close();


                const text = Deno.readTextFileSync(home+path+"editing.txt").trim();
                if(text.trim().length == 0) console.log("Nothing saved."); else console.log("Saving.");
                ntry.add_form(text);
                ntry.save();
                Deno.remove(home+path+"editing.txt");

            } else {
                let body = args.join(" ");
                for(const word of body.split(" ")){
                    if(word.endsWith("?") || word.endsWith(".") || word.endsWith("!")){
                        body = body.replace(word, word+"\n");
                        break;
                    }
                    
                }
                let tidy = [];
                for(const ln of body.split("\n")){
                    tidy.push(ln.trim());
                }

                ntry.add_form(tidy.join("\n"));
                ntry.save();
            }

            return;
        }

        case "edit": {

            let index = parseInt(String(args.shift()))
            if(args.length == 0){
                let form = ntry.forms[index];
                ensureFileSync(home+path+"editing.txt");   
                Deno.writeTextFileSync(home+path+"editing.txt", ntry.saveFriendly(form));
                const p = Deno.run({
                    cmd: [editor, home+path+"editing.txt"],
                });

                const { code } = await p.status(); 
                p.close();


                const text = Deno.readTextFileSync(home+path+"editing.txt").trim();
                if(text.trim().length == 0) console.log("Nothing saved."); else console.log("Saving.");
                ntry.add_form(text, index);
                ntry.save();
                Deno.remove(home+path+"editing.txt");

            } else {
                let body = args.join(" ");
                for(const word of body.split(" ")){
                    if(word.endsWith("?") || word.endsWith(".") || word.endsWith("!")){
                        body = body.replace(word, word+"\n");
                        break;
                    }
                    
                }
                let tidy = [];
                for(const ln of body.split("\n")){
                    tidy.push(ln.trim());
                }

                ntry.add_form(tidy.join("\n"), index);
                ntry.save();
            }
            return;
        }

        case "del":
        case "delete": {
            ntry.printForm(ntry.forms[parseInt(args[0])])
            ntry.forms.splice(parseInt(args[0]), 1);
            ntry.save()
            return;
        }

        case "encrypt": {
            // encrypts the file with a password
            return;
        }

        case "decrypt": {
            //attempts to decrypt the file with password
            return;
        }

        case "ls":
        case "list": {
            console.log("+----------------------- ");
            for (const form of ntry.forms){
                ntry.printForm(form);
                console.log("+----------------------- ");
            }
            return;
        }

        case "set": {
            let key = args[0];
            let val = args[1];
            ntry.values[key] = val;
            ntry.save();
            console.log("Value updated.")
            return;
        }

        case "get": {
            let key = args[0];
            let val = ntry.values[key];
            console.log(`${key} = ${val}`)
            return;
        }

        case "find":
        case "show": {
            console.log("+----------------------- ");
            for (const form of ntry.filtered_forms(opt.flags)){
                ntry.printForm(form);
                console.log("+----------------------- ");
            }
            return;
        }
        case "h":
        case "help":
            if(args.length == 0){
                console.log("ntry notes");
                console.log("");
                console.log("write [text]");
                console.log("edit <index> [text]");
                console.log("delete <index>");
                console.log("list");
                console.log("find <options..>");
                console.log("set <key> <value>");
                console.log("get <key>");
                console.log("");
                console.log("Use Help followed by a command for more information.");
                console.log("");
                console.log("TODO writing information here");
            } else {
                if(args[0] == "write"){
                    console.log("Adds a new entry. The default command if no command is given.");
                    console.log("If no text is given, the editor is opened, defined with the `editor` key of the file.");
                    console.log("If text is given, the header is defined as the text before the first sentence ending character.")
                } else if(args[0] == "edit"){
                    console.log("Edits and replaces an entry.");
                } else if(args[0] == "delete"){

                } else if(args[0] == "find"){

                } else if(args[0] == "set"){

                } else if(args[0] == "get"){

                } else if(args[0] == "list"){

                }
            }

        default: 
            console.log("Unknown");
    }
}

await main();