# ntry
A simple terminal note taker, inspired by jrnl.

## Running
```sh
$ ntry
```
Starts writing a new note if no sub-command is given.

```sh
$ ntry help
$ ntry list
$ ntry search --includes:"this string"
```

## Compiling
Requires Deno runtime. 
Simply run the compile bash script.